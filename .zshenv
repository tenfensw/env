#!/bin/zsh
alias coloring='osascript -e "choose color"'
alias scite='open -a SciTE'

cddev() {
	cd /Volumes/TimStorage/Dev

	if test ! -z "$1"
	then
		for subd in * .
		do
			dirv="$subd/$1"
			test -d "$dirv" && cd "$dirv"
		done
	fi
	return 0
}

finderInPWD() {
	killall Finder; open -a Finder .
	return $?
}

@() {
	test -z "$1" && cd .. && return 0
	test ! -d "$1" && mkdir -vp "$1"
	cd "$1"
	return $?
}

alias cdhell='cd ~/Documents/bureacracy'

# regular prompt
export PROMPT='%K{26}%F{15} %(?.'$'\U2705''.'$'\U26A0\UFE0F'' %?) %K{34} %~ %k%f %# '

alias .=source
alias f.='open -a Finder .'
alias ff.=finderInPWD
alias m.='mate .'
alias nano='nano -i'

# C compilers must be clang-based
export CXX=clang++
export CC=clang

test -z "$TenFen_No_fPIC" || export CFLAGS='-fPIC'
export TenFen_Ruby_Version=2.5.9

# custom prog builds
export PATH=/Library/TenFen/Ruby/$TenFen_Ruby_Version/bin:/Library/TenFen/bin:$PATH

