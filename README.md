This is literally my macOS CLI (and non-CLI) setup packaged into a few zsh scripts.

To install:

```bash

# switch to zsh
sudo chsh -s `whoami` /bin/zsh

# reconfigure macOS
/bin/zsh finderconf.sh

# install custom nano with syntax highlighting and modern features
curl -L -o- https://bitbucket.org/tenfensw/env/downloads/nano5.2-bigsur-x86_64-static.tgz | sudo tar -C / -xvzf -
sudo chown -vR `whoami`:staff /Library/TenFen
mkdir /Library/TenFen/bin
ln -vs /Library/TenFen/GNUnano/bin/nano /Library/TenFen/bin/nano

```

DimmedMonokai theme taken from https://github.com/lysyi3m/macos-terminal-themes
GNU nano is GPLv3 software, but I've only built binaries, so it doesn't matter that much
