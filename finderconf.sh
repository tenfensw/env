#!/usr/bin/env zsh
dfen() {
	operand=true
	test ! -z "$3" && test "$3" = "-no" && operand=false

	echo "$1: $2 [$operand]"
	defaults write "$1" "$2" -bool $operand
	return $?
}

dfdis() {
	dfen "$1" "$2" -no
}

restart_finder() {
	killall Finder; killall Dock; return 0
}

# detect script's parent directory
owndir=`dirname "$0"`
owndir=`env zsh -c "cd \"$owndir\" && pwd"`

# gay flag
lgbt='\U1F3F3\uFE0F\u200D\U1F308'

# crash on error
set -e

# migrate .zshenv
cp "$owndir/.zshenv" "$HOME/.zshenv"

# macOS Monterey ignores .zshenv, so we'll have to cheat a bit
touch "$HOME/.zshrc"
echo "source \"$HOME/.zshenv\"" >> "$HOME/.zshrc"

#
# Setup Finder
# - display quit menu item
# - show full posix paths in the title bar
# - show Macintosh HD on the desktop
# - ignore sorting options for folders
#
for fopt in QuitMenuItem _FXShowPosixPathInTitle ShowHardDrivesOnDesktop \
	    _FXSortFoldersFirst OpenWindowForNewRemovableDisk
do
	dfen com.apple.finder $fopt
done

# show file extensions
dfen NSGlobalDomain AppleShowAllExtensions

# pls don't move the spaces
dfdis com.apple.dock mru-spaces

# don't open safe downloads in Safari because that is just annoying
dfdis com.apple.Safari AutoOpenSafeDownloads

# enable "Inspect Element" in Safari
dfen com.apple.Safari IncludeDevelopMenu

# install "DimmedMonokai" for Terminal.app
# avoid quarantine messages from Gatekeeper
xattr -d com.apple.quarantine "$owndir/DimmedMonokai.terminal" 
open -a Terminal "$owndir/DimmedMonokai.terminal"

# turn Photos iCloud auto-import off
dfen com.apple.ImageCapture disableHotPlug

# show a pro-LGBT message every time when Terminal.app opens
printf "$lgbt  %s  $lgbt\n" 'gay rights matter' > /tmp/motd
echo "Admin password required for certain actions"
test ! -f /etc/motd && sudo mv -v /tmp/motd /etc/motd

# done
restart_finder
echo "All customizations done UwU"
exit 0
